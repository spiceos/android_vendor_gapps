# GMS core packages
PRODUCT_PACKAGES := \
    AndroidPlatformServices \
    ConfigUpdater \
    GoogleExtShared \
    GoogleFeedback \
    GoogleLocationHistory \
    GoogleOneTimeInitializer \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GooglePrintRecommendationService \
    GoogleRestore \
    GoogleServicesFramework \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    GmsCore \
    Phonesky \
    SetupWizard \
    Wellbeing \
    CalendarGoogle \
    DeskClockGoogle \
    LatinImeGoogle \
    TagGoogle \
    CalculatorGoogle \
    Velvet \
    GoogleContacts \
    GoogleDialer \
    Messages

# GMS common RRO packages
PRODUCT_PACKAGES += GmsConfigOverlayCommon GmsConfigOverlayGSA GmsConfigOverlayComms

# GMS common configuration files
PRODUCT_PACKAGES += \
    play_store_fsi_cert \
    default_permissions_allowlist_google \
    privapp_permissions_google_system \
    privapp_permissions_google_product \
    privapp_permissions_google_system_ext \
    privapp_permissions_google_comms_suite \
    split_permissions_google \
    preferred_apps_google \
    sysconfig_google \
    sysconfig_wellbeing \
    google_hiddenapi_package_allowlist

PRODUCT_PACKAGE_OVERLAYS += vendor/gapps/overlay/gms_overlay

PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.rotation_locked=true \
    setupwizard.theme=glif_v3_light \
    ro.opa.eligible_device=true \
    ro.com.google.gmsversion=11_202009
